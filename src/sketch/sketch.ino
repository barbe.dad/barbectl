#include <TaskScheduler.h>
#include <TaskSchedulerDeclarations.h>
#include <TaskSchedulerSleepMethods.h>

#include <Adafruit_NeoPixel.h>

// Define the ADC midpoint (512 is the default)
#define ADC_MIDPOINT 509

// Define the sample rate (8800 S/s is the default for 16 MHz Boards)
#define SAMPLE_RATE 8800

#include <Goertzel.h>

////////////////////////////////////////////////////////////////////////////////
// Méthodes utilitaires
////////////////////////////////////////////////////////////////////////////////

const char *uint8_t_vers_const_char(const uint8_t valeur)
{
	static const char hex[] PROGMEM = "0123456789ABCDEF";
	static char buffer[2];
	buffer[0] = pgm_read_byte_near(hex + (valeur & 0x0F));
	buffer[1] = '\0';
	return buffer;
}

void print_uint8_t(const uint8_t valeur)
{
	Serial.print(uint8_t_vers_const_char(valeur));
}

////////////////////////////////////////////////////////////////////////////////
// Code pour la communication DTMF
////////////////////////////////////////////////////////////////////////////////

const uint16_t DTMF_CAPTURE_THRESHOLD = 1000;
const uint8_t GPIO_PIN_AUDIO_L = A0;

// Array buffer for ADC samples
#define NB_SAMPLES 200
int Samples[NB_SAMPLES];

// Initialisation des capteurs de fréquence:
#define DEFINIR_FREQUENCE(hertz) const Goertzel T##hertz(hertz##.0);
DEFINIR_FREQUENCE(697);
DEFINIR_FREQUENCE(770);
DEFINIR_FREQUENCE(852);
DEFINIR_FREQUENCE(941);
DEFINIR_FREQUENCE(1209);
DEFINIR_FREQUENCE(1336);
DEFINIR_FREQUENCE(1477);
DEFINIR_FREQUENCE(1633);
#undef DEFINIR_FREQUENCE

const uint8_t NOOP = 0xFF;
uint8_t HISTORIQUE_CARA[128];
const size_t TAILLE_HISTORIQUE_CARA = 128;
const uint8_t CODE_SECRET[] PROGMEM = {0xB, 0xE, 0xE, 0xF};
const size_t TAILLE_CODE_SECRET = 4;
uint8_t CARACTERE_PRECEDENT = NOOP;

void ajouter_dans_historique(const uint8_t cara)
{
	for (size_t n = TAILLE_HISTORIQUE_CARA - 2; n > 0; n--)
	{
		HISTORIQUE_CARA[n] = HISTORIQUE_CARA[n - 1];
	}
	HISTORIQUE_CARA[0] = cara;
}

bool verifier_code_secret()
{
	for (size_t n = 0; n < TAILLE_CODE_SECRET; ++n)
	{
		if (HISTORIQUE_CARA[TAILLE_CODE_SECRET - 1 - n] != CODE_SECRET[n])
		{
			return false;
		}
	}
	return true;
}

void setup_dtmf()
{
	pinMode(GPIO_PIN_AUDIO_L, INPUT);
}

bool mode_ecoute = false;
size_t index_debut_instructions = 0;

void activer_mode_ecoute()
{
	Serial.println("code secret détecté: activation du mode écoute...");
	mode_ecoute = true;
	index_debut_instructions = 0;
}

void recevoir_instructions(const uint8_t caractere)
{
	if (!mode_ecoute)
		return;

	++index_debut_instructions;

	if (verifier_code_secret())
	{
		Serial.println("code secret détecté: fin du mode écoute");
		mode_ecoute = false;

		--index_debut_instructions;
		const size_t index_fin = TAILLE_CODE_SECRET;
		const size_t longueur_instructions = 1 + index_debut_instructions - index_fin;

		if (longueur_instructions < 1)
		{
			return;
		}

		uint8_t instructions[longueur_instructions];
		for (size_t i = index_debut_instructions, j = 0; i >= index_fin; --i, ++j)
		{
			instructions[j] = HISTORIQUE_CARA[i];
		}
		traiter_instructions(instructions, longueur_instructions);
	}
}

void loop_dtmf()
{
	// Capturer les échantillons:
	for (int n = 0; n < NB_SAMPLES; n++)
	{
		Samples[n] = analogRead(GPIO_PIN_AUDIO_L);
	}

#define VERIFIER_FREQUENCE(hertz) bool Mag##hertz = T##hertz.Mag(Samples, NB_SAMPLES) >= DTMF_CAPTURE_THRESHOLD;
	VERIFIER_FREQUENCE(697);
	VERIFIER_FREQUENCE(770);
	VERIFIER_FREQUENCE(852);
	VERIFIER_FREQUENCE(941);
	VERIFIER_FREQUENCE(1209);
	VERIFIER_FREQUENCE(1336);
	VERIFIER_FREQUENCE(1477);
	VERIFIER_FREQUENCE(1633);
#undef VERIFIER_FREQUENCE

	uint8_t caractere = NOOP;

#define OBTENIR_CARACTERE(freq1, freq2, cara)          \
	if (caractere == NOOP && Mag##freq1 && Mag##freq2) \
	{                                                  \
		caractere = cara;                              \
	}

	OBTENIR_CARACTERE(941, 1336, 0x0);
	OBTENIR_CARACTERE(697, 1209, 0x1);
	OBTENIR_CARACTERE(697, 1336, 0x2);
	OBTENIR_CARACTERE(697, 1477, 0x3);
	OBTENIR_CARACTERE(770, 1209, 0x4);
	OBTENIR_CARACTERE(770, 1336, 0x5);
	OBTENIR_CARACTERE(770, 1477, 0x6);
	OBTENIR_CARACTERE(852, 1209, 0x7);
	OBTENIR_CARACTERE(852, 1336, 0x8);
	OBTENIR_CARACTERE(852, 1477, 0x9);
	OBTENIR_CARACTERE(697, 1633, 0xA);
	OBTENIR_CARACTERE(770, 1633, 0xB);
	OBTENIR_CARACTERE(852, 1633, 0xC);
	OBTENIR_CARACTERE(941, 1633, 0xD);
	OBTENIR_CARACTERE(941, 1209, 0xE);
	OBTENIR_CARACTERE(941, 1477, 0xF);
#undef OBTENIR_CARACTERE

	if (caractere != NOOP && caractere != CARACTERE_PRECEDENT)
	{
		if (CARACTERE_PRECEDENT == NOOP)
		{
			ajouter_dans_historique(caractere);
			if (mode_ecoute)
			{
				recevoir_instructions(caractere);
			}
			else if (verifier_code_secret())
			{
				activer_mode_ecoute();
			}
			print_uint8_t(caractere);
			Serial.println();
		}
	}
	CARACTERE_PRECEDENT = caractere;
}

////////////////////////////////////////////////////////////////////////////////
// Code pour le contrôle des lumières
////////////////////////////////////////////////////////////////////////////////
const uint8_t NUM_LEDS = 8;
const uint8_t GPIO_PIN_ANNEAU = 11;
const uint8_t GPIO_PIN_VCC = 7;

const Adafruit_NeoPixel anneauLumiere = Adafruit_NeoPixel(NUM_LEDS, GPIO_PIN_ANNEAU, NEO_GRB + NEO_KHZ800);
const Scheduler scheduler;

const uint32_t CARACTERE_DTMF_VERS_COULEUR[] = {
	anneauLumiere.Color(0, 0, 0),		// noir
	anneauLumiere.Color(255, 0, 0),		// rouge
	anneauLumiere.Color(255, 128, 0),	// orange
	anneauLumiere.Color(255, 255, 0),	// jaune
	anneauLumiere.Color(0, 255, 0),		// vert
	anneauLumiere.Color(0, 255, 255),	// aqua
	anneauLumiere.Color(0, 0, 255),		// bleu
	anneauLumiere.Color(90, 0, 255),	// violet
	anneauLumiere.Color(255, 0, 255),	// magenta
	anneauLumiere.Color(255, 255, 255), // blanc
};
const size_t NB_COULEURS = 10;

uint32_t nombreVersCouleur(const uint8_t nombre)
{
	if (nombre >= NB_COULEURS)
	{
		return 0;
	}
	return CARACTERE_DTMF_VERS_COULEUR[nombre];
}

const uint32_t TOUS_ETEINTS[NUM_LEDS] = {
	CARACTERE_DTMF_VERS_COULEUR[0],
	CARACTERE_DTMF_VERS_COULEUR[0],
	CARACTERE_DTMF_VERS_COULEUR[0],
	CARACTERE_DTMF_VERS_COULEUR[0],
	CARACTERE_DTMF_VERS_COULEUR[0],
	CARACTERE_DTMF_VERS_COULEUR[0],
	CARACTERE_DTMF_VERS_COULEUR[0],
	CARACTERE_DTMF_VERS_COULEUR[0],
};
uint32_t THEME1[NUM_LEDS] = {0, 0, 0, 0, 0, 0, 0, 0};
uint32_t THEME2[NUM_LEDS] = {0, 0, 0, 0, 0, 0, 0, 0};

const uint16_t CARACTERE_DTMF_VERS_VITESSE[] = {
	0,
	3, // lent
	2, // moyen
	1, // rapide
};
const size_t NB_VITESSES = 4;

uint16_t nombreVersVitesse(const uint8_t nombre)
{
	if (nombre >= NB_VITESSES)
	{
		return CARACTERE_DTMF_VERS_VITESSE[1];
	}
	return CARACTERE_DTMF_VERS_VITESSE[nombre];
}

uint16_t VITESSE = nombreVersVitesse(0);

void envoyerCouleurs(const uint32_t couleurs[], const uint8_t luminosite = 255)
{
	anneauLumiere.setBrightness(luminosite);
	for (uint8_t i = 0; i < NUM_LEDS; i++)
	{
		anneauLumiere.setPixelColor(i, couleurs[i]);
	}
	anneauLumiere.show();
}

void traiter_instructions(const uint8_t instructions[], const size_t nb_instructions)
{
	const uint8_t operation = instructions[0];

	annuler_tasks();

	switch (operation)
	{
	case 0x0:
		traiter_afficher(instructions, nb_instructions);
		break;
	case 0xE:
		traiter_alternance(instructions, nb_instructions);
		break;
	case 0xA:
		traiter_pulsation(instructions, nb_instructions);
		break;
	case 0xB:
		traiter_fondu(instructions, nb_instructions);
		break;
	case 0xC:
		traiter_rotation(instructions, nb_instructions);
		break;
	case 0xD:
		traiter_clignottement(instructions, nb_instructions);
		break;
	default:
		Serial.print("/!\\ Opération 0x");
		print_uint8_t(operation);
		Serial.println(" non reconnue! Les instructions seront ignorées.");
		break;
	}
}

void traiter_afficher(const uint8_t instructions[], const size_t nb_instructions)
{
	if (nb_instructions < (1 + NUM_LEDS))
	{
		Serial.print("/!\\ Pas assez d'instructions pour programmer les ");
		print_uint8_t(NUM_LEDS);
		Serial.println(" couleurs! Les instructions seront ignorées.");
		return;
	}

	for (uint8_t i = 0; i < NUM_LEDS; ++i)
	{
		THEME1[i] = nombreVersCouleur(instructions[i + 1]);
	}

	envoyerCouleurs(THEME1);
}

Task pulsationTaskAsc(300, 13, &pulsationAsc);
Task pulsationTaskDesc(300, 13, &pulsationDesc);

void pulsationAsc()
{
	if (pulsationTaskAsc.isFirstIteration())
	{
		pulsationTaskDesc.abort();
	}

	const uint8_t i = (uint8_t)pulsationTaskAsc.getRunCounter();
	const uint8_t luminosite = (uint8_t)constrain(i * 20, 0, 255);
	envoyerCouleurs(THEME1, luminosite);

	if (pulsationTaskAsc.isLastIteration())
	{
		pulsationTaskDesc.restart();
	}
}

void pulsationDesc()
{
	if (pulsationTaskDesc.isFirstIteration())
	{
		pulsationTaskAsc.abort();
	}

	const uint8_t i = (uint8_t)pulsationTaskDesc.getRunCounter();
	const uint8_t luminosite = (uint8_t)(255 - constrain(i * 20, 0, 255));
	envoyerCouleurs(THEME1, luminosite);

	if (pulsationTaskDesc.isLastIteration())
	{
		pulsationTaskAsc.restart();
	}
}

void traiter_pulsation(const uint8_t instructions[], const size_t nb_instructions)
{
	if (nb_instructions < (1 + NUM_LEDS + 1))
	{
		Serial.print("/!\\ Pas assez d'instructions pour programmer les ");
		print_uint8_t(NUM_LEDS);
		Serial.println(" couleurs et la vitesse! Les instructions seront ignorées.");
		return;
	}

	for (uint8_t i = 0; i < NUM_LEDS; ++i)
	{
		THEME1[i] = nombreVersCouleur(instructions[i + 1]);
	}

	VITESSE = nombreVersVitesse(instructions[NUM_LEDS + 1]);
	pulsationTaskAsc.setInterval((unsigned long)(VITESSE * 80));
	pulsationTaskDesc.setInterval((unsigned long)(VITESSE * 80));

	pulsationTaskAsc.restart();
}

Task rotationTask(30, TASK_FOREVER, &rotation);
void rotation()
{
	const uint32_t temp = THEME1[NUM_LEDS - 1];
	for (uint8_t i = NUM_LEDS - 1; i > 0; --i)
	{
		THEME1[i] = THEME1[i - 1];
	}
	THEME1[0] = temp;
	envoyerCouleurs(THEME1);
}

void traiter_rotation(const uint8_t instructions[], const size_t nb_instructions)
{
	if (nb_instructions < (1 + NUM_LEDS + 1))
	{
		Serial.print("/!\\ Pas assez d'instructions pour programmer les ");
		print_uint8_t(NUM_LEDS);
		Serial.println(" couleurs et la vitesse! Les instructions seront ignorées.");
		return;
	}

	for (uint8_t i = 0; i < NUM_LEDS; ++i)
	{
		THEME1[i] = nombreVersCouleur(instructions[i + 1]);
	}

	VITESSE = nombreVersVitesse(instructions[NUM_LEDS + 1]);
	rotationTask.setInterval((unsigned long)(VITESSE * 200));

	rotationTask.restart();
}

Task alternanceTask(30, TASK_FOREVER, &alternance);
void alternance()
{
	envoyerCouleurs(alternanceTask.getRunCounter() % 2 ? THEME1 : THEME2);
}

void traiter_alternance(const uint8_t instructions[], const size_t nb_instructions)
{
	if (nb_instructions < (1 + NUM_LEDS + NUM_LEDS + 1))
	{
		Serial.print("/!\\ Pas assez d'instructions pour programmer les ");
		print_uint8_t(NUM_LEDS * 2);
		Serial.println(" couleurs et la vitesse! Les instructions seront ignorées.");
		return;
	}

	for (uint8_t i = 0; i < NUM_LEDS; ++i)
	{
		THEME1[i] = nombreVersCouleur(instructions[i + 1]);
	}

	for (uint8_t i = 0; i < NUM_LEDS; ++i)
	{
		THEME2[i] = nombreVersCouleur(instructions[i + 1 + NUM_LEDS]);
	}

	VITESSE = nombreVersVitesse(instructions[NUM_LEDS + NUM_LEDS + 1]);
	alternanceTask.setInterval((unsigned long)(VITESSE * 200));

	alternanceTask.restart();
}

Task clignottementTask(600, TASK_FOREVER, &clignottement);
void clignottement()
{
	envoyerCouleurs(clignottementTask.getRunCounter() % 2 ? THEME1 : (uint32_t *)TOUS_ETEINTS);
}

void traiter_clignottement(const uint8_t instructions[], const size_t nb_instructions)
{
	if (nb_instructions < (1 + NUM_LEDS + 1))
	{
		Serial.print("/!\\ Pas assez d'instructions pour programmer les ");
		print_uint8_t(NUM_LEDS);
		Serial.println(" couleurs et la vitesse! Les instructions seront ignorées.");
		return;
	}

	for (uint8_t i = 0; i < NUM_LEDS; ++i)
	{
		THEME1[i] = nombreVersCouleur(instructions[i + 1]);
	}

	VITESSE = nombreVersVitesse(instructions[NUM_LEDS + 1]);
	clignottementTask.setInterval(VITESSE * 200);

	clignottementTask.restart();
}

Task fonduTaskAsc1(300, 13, &fonduAsc1);
Task fonduTaskDesc1(300, 13, &fonduDesc1);
Task fonduTaskAsc2(300, 13, &fonduAsc2);
Task fonduTaskDesc2(300, 13, &fonduDesc2);

void fonduAsc1()
{
	if (fonduTaskAsc1.isFirstIteration())
	{
		fonduTaskDesc1.abort();
		fonduTaskAsc2.abort();
		fonduTaskDesc2.abort();
	}

	const uint8_t i = (uint8_t)fonduTaskAsc1.getRunCounter();
	const uint8_t luminosite = (uint8_t)constrain(i * 20, 0, 255);
	envoyerCouleurs(THEME1, luminosite);

	if (fonduTaskAsc1.isLastIteration())
	{
		fonduTaskDesc1.restart();
	}
}

void fonduDesc1()
{
	if (fonduTaskDesc1.isFirstIteration())
	{
		fonduTaskAsc1.abort();
		fonduTaskAsc2.abort();
		fonduTaskDesc2.abort();
	}

	const uint8_t i = (uint8_t)fonduTaskDesc1.getRunCounter();
	const uint8_t luminosite = (uint8_t)(255 - constrain(i * 20, 0, 255));
	envoyerCouleurs(THEME1, luminosite);

	if (fonduTaskDesc1.isLastIteration())
	{
		fonduTaskAsc2.restart();
	}
}

void fonduAsc2()
{
	if (fonduTaskAsc2.isFirstIteration())
	{
		fonduTaskDesc1.abort();
		fonduTaskAsc1.abort();
		fonduTaskDesc2.abort();
	}

	const uint8_t i = (uint8_t)fonduTaskAsc2.getRunCounter();
	const uint8_t luminosite = (uint8_t)constrain(i * 20, 0, 255);
	envoyerCouleurs(THEME2, luminosite);

	if (fonduTaskAsc2.isLastIteration())
	{
		fonduTaskDesc2.restart();
	}
}

void fonduDesc2()
{
	if (fonduTaskDesc2.isFirstIteration())
	{
		fonduTaskAsc1.abort();
		fonduTaskAsc2.abort();
		fonduTaskDesc1.abort();
	}

	const uint8_t i = (uint8_t)fonduTaskDesc2.getRunCounter();
	const uint8_t luminosite = (uint8_t)(255 - constrain(i * 20, 0, 255));
	envoyerCouleurs(THEME2, luminosite);

	if (fonduTaskDesc2.isLastIteration())
	{
		fonduTaskAsc1.restart();
	}
}

void traiter_fondu(const uint8_t instructions[], const size_t nb_instructions)
{
	if (nb_instructions < (1 + NUM_LEDS + NUM_LEDS + 1))
	{
		Serial.print("/!\\ Pas assez d'instructions pour programmer les ");
		print_uint8_t(NUM_LEDS * 2);
		Serial.println(" couleurs et la vitesse! Les instructions seront ignorées.");
		return;
	}

	for (uint8_t i = 0; i < NUM_LEDS; ++i)
	{
		THEME1[i] = nombreVersCouleur(instructions[i + 1]);
	}

	for (uint8_t i = 0; i < NUM_LEDS; ++i)
	{
		THEME2[i] = nombreVersCouleur(instructions[i + 1 + NUM_LEDS]);
	}

	VITESSE = nombreVersVitesse(instructions[NUM_LEDS + NUM_LEDS + 1]);
	fonduTaskAsc1.setInterval((unsigned long)(VITESSE * 80));
	fonduTaskDesc1.setInterval((unsigned long)(VITESSE * 80));
	fonduTaskAsc2.setInterval((unsigned long)(VITESSE * 80));
	fonduTaskDesc2.setInterval((unsigned long)(VITESSE * 80));

	fonduTaskAsc1.restart();
}

void annuler_tasks()
{
	Serial.println("Annulation de toutes les tâches...");
	pulsationTaskAsc.abort();
	pulsationTaskDesc.abort();
	fonduTaskAsc1.abort();
	fonduTaskDesc1.abort();
	fonduTaskAsc2.abort();
	fonduTaskDesc2.abort();
	rotationTask.abort();
	alternanceTask.abort();
	clignottementTask.abort();
}

void setup_leds()
{
	pinMode(GPIO_PIN_VCC, OUTPUT);
	digitalWrite(GPIO_PIN_VCC, HIGH);
	anneauLumiere.begin();

	scheduler.addTask(pulsationTaskAsc);
	scheduler.addTask(pulsationTaskDesc);
	scheduler.addTask(fonduTaskAsc1);
	scheduler.addTask(fonduTaskDesc1);
	scheduler.addTask(fonduTaskAsc2);
	scheduler.addTask(fonduTaskDesc2);
	scheduler.addTask(rotationTask);
	scheduler.addTask(alternanceTask);
	scheduler.addTask(clignottementTask);
	annuler_tasks();
}

void loop_leds()
{
	scheduler.execute();
}

////////////////////////////////////////////////////////////////////////////////
// Initialisation
////////////////////////////////////////////////////////////////////////////////

void setup()
{
	Serial.begin(9600);
	setup_leds();
	setup_dtmf();
}

////////////////////////////////////////////////////////////////////////////////
// Boucle principale
////////////////////////////////////////////////////////////////////////////////

void loop()
{
	loop_leds();
	loop_dtmf();
}